<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CvController extends Controller
{
    
    public function addview()
    {
        return view('cv.add_cv');
    }

    public function getAll()
    {
        $liste_cvs=Cv::all();
        return view('cv.list', ['liste_cvs'=>$liste_cvs]);
    }

    public function edit($id) 
    {
        return view('cv.edit');
    }

    public function update()
    {
        return $this->getAll();
    }

public function delete($id)
    {
        return $this->getAll();
    }

    public function upload(Request $request)
    {
        $cv=new cv;
        $cv->nom=$request->nom;
        $cv->prenom=$request->prenom;
        $cv->age=$request->age;
        $cv->adress=$request->adress;
        $cv->email=$request->email;
        $cv->phone=$request->phone;
        $cv->experiance=$request->experiance;
        $cv->niveau=$request->niveau;
        $cv->save();

        return redirect()->back();

       

    }

}
