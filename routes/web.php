<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout');
});



Route::get('/home',[HomeController::class,'redirect']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Route::get('/add_emploi_view',[AdminController::class,'addview']);
Route::get('/add_cv_view',[UserController::class,'addview']);

Route::post('/upload_cv',[UserController::class,'upload']);

//Route::get('/list',[UserController::class,'list']);



Route::get('/layout','UserController@index')->name('layout');

Route::get('/user/add_cv','UserController@addview')->name('addview');
Route::get('/user/edit/{id}','UserController@edit')->name('edituser');
Route::post('/user/update','UserController@update')->name('updateuser');
Route::get('/user/delete/{id}','UserController@delete')->name('deleteuser');
//Route::get('/user/getAll','UserController@getAll')->name('getalluser');

Route::post('/user/upload','UserController@upload')->name('uploaduser');


Route::get('/cv/add_cv','CvController@addview')->name('add_cvcv');
Route::get('/cv/edit/{id}','CvController@edit')->name('editcv');
Route::post('/cv/update','CvController@update')->name('updatecv');
Route::get('/cv/delete/{id}','CvController@delete')->name('deletecv');
Route::get('/cv/getAll','CvController@getAll')->name('getallcv');

Route::get('/user/getAll', 'App\Http\Controllers\UserController@getAll');